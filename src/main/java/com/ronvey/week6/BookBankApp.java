package com.ronvey.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank Punya = new BookBank();
        Punya.name = "Punya";
        Punya.balance = 100.0;
        Punya.print();
        Punya.deposit(50);
        Punya.print();
        Punya.withdraw(50);
        Punya.print();

        BookBank pradu = new BookBank();
        pradu.name = "pradu";
        pradu.balance = 1;
        pradu.deposit(1000000);
        pradu.withdraw(10000000);
        pradu.print();

        BookBank pradi = new BookBank();
        pradi.name = "PraDii";
        pradi.balance = 10;
        pradi.print();
        pradi.deposit(10000000);
        pradi.withdraw(1000000);

    }

}
