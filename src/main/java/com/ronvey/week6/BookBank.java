package com.ronvey.week6;

public class BookBank {
    String name;
    double balance;

    boolean deposit(double money) {
        if (money <= 0) {
            return false;
        }
        balance = balance + money;
        return true;
    }

    boolean withdraw(double money) {
        if (money <= 0) {
            return false;
        }
        if (money > balance) {
            return false;
        }
        balance = balance - money;
        return true;
    }

    void print() {
        System.out.println(name + ' ' + balance);
    }

    public String getName() {
        return name;
    }

    public double getBalbance() {
        return balance;
    }
}
